$(document).foundation();

$('.bxslider').bxSlider({
  minSlider: 4,
  maxSlides: 4,
  slideWidth: 344.5,
});

$( function() {
  $( ".datepicker" ).datepicker( $.datepicker.regional[ "cs" ] );
  $( ".datepicker" ).datepicker({
  	dateFormat: "dd.mm.yy"
  });
} );